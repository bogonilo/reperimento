base = '/Users/daniele/Desktop/reperimento1';
pathSum = {strcat(base,'/runs_results/result/combsum.res')};
pathMax = {strcat(base,'/runs_results/result/combmax.res')};
pathMin = {strcat(base,'/runs_results/result/combmin.res')};
pathMed = {strcat(base,'/runs_results/result/combmed.res')};
pathMnz = {strcat(base,'/runs_results/result/combmnz.res')};
pathAnz = {strcat(base,'/runs_results/result/combanz.res')};
pathCon = {strcat(base,'/runs_results/result/condor_NEW.res')};
pathBM25 = {strcat(base,'/runs_results/run/BM25.res')};
pathBM25s = {strcat(base,'/runs_results/run/BM25sw.res')};
pathTF = {strcat(base,'/runs_results/run/TF_IDF.res')};
pathTFs = {strcat(base,'/runs_results/run/TF_IDFsw.res')};

poolFileName = {strcat(base,'/share/qrels.trec')};
collectionID = 'BM_25';
relevanceDegrees = {'NotRelevant','Relevant'};
relevanceGrades = 0:1 ;
outputPath = [base];
runSetPath = [base];
runSetDelimeter = 'space';
delimeter = 'space';
singlePrecision = true;
start = tic;

fprintf('\n\n Importo la collezione : %s \n\n', collectionID);

fprintf('\n\n Importo le run...\n');
runSum = importRunFromFileTRECFormat('FileName', pathSum, 'Delimiter', 'space', 'DocumentOrdering', 'Matters');
runMax = importRunFromFileTRECFormat('FileName', pathMax, 'Delimiter', 'space', 'DocumentOrdering', 'Matters');
runMin = importRunFromFileTRECFormat('FileName', pathMin, 'Delimiter', 'space', 'DocumentOrdering', 'Matters');
runMnz = importRunFromFileTRECFormat('FileName', pathMnz, 'Delimiter', 'space', 'DocumentOrdering', 'Matters');
runMed = importRunFromFileTRECFormat('FileName', pathMed, 'Delimiter', 'space', 'DocumentOrdering', 'Matters');
runAnz = importRunFromFileTRECFormat('FileName', pathAnz, 'Delimiter', 'space', 'DocumentOrdering', 'Matters');
runCon = importRunFromFileTRECFormat('FileName', pathCon, 'Delimiter', 'space', 'DocumentOrdering', 'Matters');
runBM25 = importRunFromFileTRECFormat('FileName', pathBM25, 'Delimiter', 'space', 'DocumentOrdering', 'Matters');
runBM25s = importRunFromFileTRECFormat('FileName', pathBM25, 'Delimiter', 'space', 'DocumentOrdering', 'Matters');
runTFs = importRunFromFileTRECFormat('FileName', pathTFs, 'Delimiter', 'space', 'DocumentOrdering', 'Matters');
runTF = importRunFromFileTRECFormat('FileName', pathTF, 'Delimiter', 'space', 'DocumentOrdering', 'Matters');


fprintf('\n\n Importate!');

pool = importPoolFromFileTRECFormat('FileName', poolFileName, 'Identifier', collectionID,...
'RelevanceGrades', relevanceGrades, 'RelevanceDegrees', relevanceDegrees);

fprintf('\n\n Collezione importata!\n\n');

%%

%AREA DISCOUNTED CUMULATIVE GAIN
x = [5 10 15 20 30 100 200 500 1000];

numTopic = '353';

%It returns the discounted cumulated gain at 5, 10, 15, 20, 30, 100,
%200,...500, and 1000 retrieved documents for all topic of run combSum
dcgSum = discountedCumulatedGain(pool, runSum, 'CutOffs', 'Standard');
sum353 = dcgSum{numTopic,'combSum'};

dcgAnz = discountedCumulatedGain(pool, runAnz, 'CutOffs', 'Standard');
anz353 = dcgAnz{numTopic,'combAnz'};

dcgMax = discountedCumulatedGain(pool, runMax, 'CutOffs', 'Standard');
max353 = dcgMax{numTopic,'combMax'};

dcgMin = discountedCumulatedGain(pool, runMin, 'CutOffs', 'Standard');
min353 = dcgMin{numTopic,'combMin'};

dcgMed = discountedCumulatedGain(pool, runMed, 'CutOffs', 'Standard');
med353 = dcgMed{numTopic,'combMed'};

dcgMnz = discountedCumulatedGain(pool, runMnz, 'CutOffs', 'Standard');
mnz353 = dcgMnz{numTopic,'combMnz'};

dcgCon = discountedCumulatedGain(pool, runCon, 'CutOffs', 'Standard');
con353 = dcgCon{numTopic,'condor'};

dcgBM25 = discountedCumulatedGain(pool, runBM25, 'CutOffs', 'Standard');
bm353 = dcgBM25{numTopic,'matters_BM25b10'};


plot(x, sum353, 'm', x, anz353, 'c', x, max353, 'r', x, min353, 'g', x, med353, 'b', x, mnz353, 'y',...
    x, con353, '--.b', x, bm353, 'k')
xlabel('Cut-off')
ylabel('DCG')
title(strcat('DCG all rank fusion systems for topic ', numTopic))
legend('combSUM','combANZ','combMAX','combMIN','combMED','combMNZ','Condorcet','BM25','Location','southeast');

%%

%AREA PRECISION
x = [5 10 15 20 30 100 200 500 1000];

numTopic = '353';

precSum = precision(pool, runSum, 'CutOffs', 'Standard');
sum353 = precSum{numTopic,'combSum'};

precMax = precision(pool, runMax, 'CutOffs', 'Standard');
max353 = precMax{numTopic,'combMax'};

precMin = precision(pool, runMin, 'CutOffs', 'Standard');
min353 = precMin{numTopic,'combMin'};

precMnz = precision(pool, runMnz, 'CutOffs', 'Standard');
mnz353 = precMnz{numTopic,'combMnz'};

precMed = precision(pool, runMed, 'CutOffs', 'Standard');
med353 = precMed{numTopic,'combMed'};

precAnz = precision(pool, runAnz, 'CutOffs', 'Standard');
anz353 = precAnz{numTopic,'combAnz'};

precCon = precision(pool, runCon, 'CutOffs', 'Standard');
con353 = precCon{numTopic,'condor'};

precBM25 = precision(pool, runBM25, 'CutOffs', 'Standard');
bm353 = precBM25{numTopic,'matters_BM25b10'};


plot(x, sum353, 'm', x, anz353, 'c', x, max353, 'r', x, min353, 'g', x, med353, 'b', x, mnz353, 'y',...
    x, con353, '--.b', x, bm353, 'k')
xlabel('Cut-off')
ylabel('Precision')
title(strcat('Precision of all rank fusion systems for topic ', numTopic))
legend('combSUM','combANZ','combMAX','combMIN','combMED','combMNZ','Condorcet','BM25','Location','northeast');

%%

%AREA AVERAGE PRECISION
x = [370 371 372 373 374 375 376 377 378 379 380];

precSum = averagePrecision(pool, runSum);
sum353 = precSum{20:30,'combSum'};

sum1 = 0;
for j=1:50
    sum1 = sum1 + precSum{j,'combSum'};
end
mapSum = (sum1/50);

precMax = averagePrecision(pool, runMax);
max353 = precMax{20:30,'combMax'};

sum1 = 0;
for j=1:50
    sum1 = sum1 + precMax{j,'combMax'};
end
mapMax = (sum1/50);

precMin = averagePrecision(pool, runMin);
min353 = precMin{20:30,'combMin'};

sum1 = 0;
for j=1:50
    sum1 = sum1 + precMin{j,'combMin'};
end
mapMin = (sum1/50);

precMnz = averagePrecision(pool, runMnz);
mnz353 = precMnz{20:30,'combMnz'};

sum1 = 0;
for j=1:50
    sum1 = sum1 + precMnz{j,'combMnz'};
end
mapMnz = (sum1/50);

precMed = averagePrecision(pool, runMed);
med353 = precMed{20:30,'combMed'};

sum1 = 0;
for j=1:50
    sum1 = sum1 + precMed{j,'combMed'};
end
mapMed = (sum1/50);

precAnz = averagePrecision(pool, runAnz);
anz353 = precAnz{20:30,'combAnz'};

sum1 = 0;
for j=1:50
    sum1 = sum1 + precAnz{j,'combAnz'};
end
mapAnz = (sum1/50);

precCon = averagePrecision(pool, runCon);
con353 = precCon{20:30,'condor'};

sum1 = 0;
for j=1:50
    sum1 = sum1 + precCon{j,'condor'};
end
mapCon = (sum1/50);

precBM25 = averagePrecision(pool, runBM25);
bm353 = precBM25{20:30,'matters_BM25b10'};

sum1 = 0;
for j=1:50
    sum1 = sum1 + precBM25{j,'matters_BM25b10'};
end
mapBM = (sum1/50);

apTF = averagePrecision(pool, runTF);

sum1 = 0;
for j=1:50
    sum1 = sum1 + apTF{j,'TF_IDF'};
end
mapTF = (sum1/50);

plot(x, sum353, 'm', x, anz353, 'c', x, max353, 'r', x, min353, 'g', x, med353, 'b', x, mnz353, 'y',...
    x, con353, '--.b', x, bm353, 'k')
xlabel('Topic')
ylabel('Average Precision')
title('Average Precision all rank fusion systems for topic range 370-380')
legend('combSUM','combANZ','combMAX','combMIN','combMED','combMNZ','Condorcet','BM25','Location','northeast');

%%
%GRAFICO MAP

y = [0 mapSum mapAnz mapMax mapMin mapMed mapMnz mapCon mapBM mapTF 0];

impz(y)

axis([0 10 0.13 0.20])
xlabel('')
ylabel('MAP')
title('MAP value comparation')
xticklabels({'', 'cSum','cAnz','cMax','cMin','cMed','cMnz','condor','BM25','TFIDF',''});

%%

%AREA DISCOUNTED CUMULATIVE GAIN RUN
x = [5 10 15 20 30 100 200 500 1000];

numTopic = '353';

%It returns the discounted cumulated gain at 5, 10, 15, 20, 30, 100,
%200,...500, and 1000 retrieved documents for all topic of run combSum

dcgCon = discountedCumulatedGain(pool, runCon, 'CutOffs', 'Standard');
con353 = dcgCon{numTopic,'condor'};

dcgBM25 = discountedCumulatedGain(pool, runBM25, 'CutOffs', 'Standard');
bm = dcgBM25{numTopic,'matters_BM25b10'};

dcgBM25s = discountedCumulatedGain(pool, runBM25s, 'CutOffs', 'Standard');
bms = dcgBM25{numTopic,'matters_BM25b10'};

dcgTF = discountedCumulatedGain(pool, runTF, 'CutOffs', 'Standard');
tf = dcgTF{numTopic,'TF_IDF'};

dcgTFs = discountedCumulatedGain(pool, runTFs, 'CutOffs', 'Standard');
tfs = dcgTFs{numTopic,'TF_IDF'};

plot(x, con353, '--.b', x, bm, 'g', x, tf, 'm')
xlabel('Cut-off')
ylabel('DCG')
title(strcat('DCG of BM25, TFIDF, Condorcet for topic ', numTopic))
legend('Condorcet','BM25','TFIDF','Location','southeast');
