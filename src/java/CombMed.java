import java.io.*;
import java.util.Scanner;
import java.util.Collections;

public class CombMed
{
    public static void main(String[] args)
    {
        int righeTotaliStampate = 0;
        String[] nomeFile = {"BM25", "BM25sw", "DFIZ", "DFIZsw", "InL2", "InL2sw", "PL2", "PL2sw", "TF_IDF", "TF_IDFsw"};
        try
        {
            BufferedWriter bw = new BufferedWriter(new FileWriter("runs_results/result/combmed.res"));
            BufferedReader[] br = new BufferedReader[10];
            for(int i = 0; i < 10; i++){
                br[i] = new BufferedReader(new FileReader("runs_results/normalized/normalized" + nomeFile[i] + ".res"));
            }
            int[][] contatori = new int[10][50]; //per ogni run quanti documenti ci sono per ogni topic
            String riga;
            int contatoreElementi = 0;
            int contatoreTopic = 351;
            int indice = 0;

            int i;
            for(i = 0; i < 10; i++){
                contatoreElementi = 0;
                indice = 0;
                contatoreTopic = 351;
                while ((riga = br[i].readLine()) != null) {
                    Scanner scan = new Scanner(riga);

                    int topic = Integer.parseInt(scan.next());
                    //System.out.println(topic + " ---- " + contatoreTopic);
                    if(topic == contatoreTopic){
                        contatoreElementi++;
                    }
                    else if(topic == contatoreTopic + 1){
                        System.out.println("entrato");
                        contatori[i][indice] = contatoreElementi;
                        contatoreTopic++;
                        indice++;
                        System.out.println(indice + " con elementi: " + contatoreElementi);
                        contatoreElementi = 1;
                    }
                }
                contatori[i][indice] = contatoreElementi;
                indice++;
                System.out.println(indice + " con elementi: " + contatoreElementi);
            }


            System.out.println("Fine ricerca contatori");

            br = new BufferedReader[10];

            for(i = 0; i < 10; i++) {
                br[i] = new BufferedReader(new FileReader("runs_results/normalized/normalized" + nomeFile[i] + ".res"));
            }

            int z = 0;

            List<Documento> classifica;

            for(int all = 0; all < 50; all++)
            {

                classifica = new ArrayList<Documento>();

                for (int iFile = 0; iFile < 10; iFile++)
                {

                    int contatoreRighe = 0;

                    while (contatoreRighe < contatori[iFile][z])
                    {

                        riga = br[iFile].readLine();
                        contatoreRighe++;
                        Scanner scan = new Scanner(riga);

                        int topic = Integer.parseInt(scan.next());
                        String numDoc = scan.next();
                        scan.next();
                        double score = Double.parseDouble(scan.next());

                        boolean trovato = false;
                        for (int j = 0; (j < classifica.size()) && (!trovato); j++) {
                            if ((classifica.get(j).getNumDoc().equalsIgnoreCase(numDoc)) && (classifica.get(j).getTopic() == topic)) {
                                trovato = true;
                                classifica.get(j).addElement(score);
                            }
                        }
                        if (!trovato) {
                            Documento d1 = new Documento(topic, numDoc, score, "array");
                            classifica.add(d1);
                        }
                    }
                }

                for(int ordine = 0; ordine < classifica.size(); ordine++){
                    classifica.get(ordine).trovaMed();
                }
                //System.out.println("Ordinamento iniziato");
                Collections.sort(classifica);
                //System.out.println("Ordinamento finito");

                System.out.print("Stampato topic: " + classifica.get(0).getTopic());

                int stampa;
                for(stampa = 0; (stampa < classifica.size()) && (stampa < 1000); stampa++)
                {
                    Documento d1 = classifica.get(stampa);
                    bw.write(d1.getTopic() + " Q0 " + d1.getNumDoc() + " " + stampa + " " + d1.getScore() + " combMed \n");
                    bw.flush();
                    righeTotaliStampate++;
                }
                System.out.print(" Stampate: " + stampa + "\n");


                z++;
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        System.out.print("Righe Totali Stampate: " + righeTotaliStampate + "\n");
    }
}
