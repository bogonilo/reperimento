import java.io.*;
import java.util.Scanner;

public class Normalizer
{
    /**
     * Non per tutti i topic ci sono 1000 risultati (???)
     * Per il topic 369 ci sono solo 70 risultati(???)
     */
    public static void main(String[] args)
    {
        String nomeFile;
        String riga;
        try
        {
            BufferedReader brInput = new BufferedReader(new InputStreamReader(System.in));
            BufferedReader br;
            BufferedReader brMinMax;
            BufferedWriter bw2;
            Scanner s;
            Scanner sMinMax;
            while(true)
            {
                System.out.println("Inserisci il nome del file da normalizzare: ");
                nomeFile = brInput.readLine();

                br = new BufferedReader(new FileReader("runs_results/run/" + nomeFile));
                brMinMax = new BufferedReader(new FileReader("runs_results/min_max/" + nomeFile + "_min_max"));
                bw2 = new BufferedWriter(new FileWriter("runs_results/normalized/normalized" + nomeFile));

                int contatore = -1;
                double score = 0;

                String rigaMinMax = brMinMax.readLine();
                sMinMax = new Scanner(rigaMinMax);
                int topicAttualeMinMax = Integer.parseInt(sMinMax.next());
                double max = Double.parseDouble(sMinMax.next());
                double min = Double.parseDouble(sMinMax.next());
                double difference = max - min;

                while ((riga = br.readLine()) != null)
                {
                    s = new Scanner(riga);
                    int topicAttuale = Integer.parseInt(s.next());

                    s.next();
                    String idDoc = s.next();
                    contatore = Integer.parseInt(s.next());
                    score = Double.parseDouble(s.next());

                    if(topicAttualeMinMax == topicAttuale - 1){
                        rigaMinMax = brMinMax.readLine();
                        sMinMax = new Scanner(rigaMinMax);
                        topicAttualeMinMax = Integer.parseInt(sMinMax.next());
                        max = Double.parseDouble(sMinMax.next());
                        min = Double.parseDouble(sMinMax.next());
                        difference = max - min;
                    }
                    score = (score - min) / (difference);
                    bw2.write(topicAttuale + " " + idDoc + " " + contatore + " " + score + "\n");
                    bw2.flush();
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
