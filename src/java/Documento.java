import java.util.ArrayList;
import java.util.List;

class Documento implements Comparable<Documento>{
    private int topic;
    private String numDoc;
    private double score;
    private int contatore;
    private List<Double> elementi;
    private int contatoreElementi;
    private int contaRank;

    public Documento(int topic, String numDoc, double score){
        this.topic = topic;
        this.numDoc = numDoc;
        this.score = score;
        contatore = 1;
    }

    public Documento(int topic, String numDoc, double score, String tipo){
        this.topic = topic;
        this.numDoc = numDoc;
        contatore = 1;
        elementi = new ArrayList<Double>();
        elementi.add(score);
    }

    public Documento(int topic, String numDoc, double score, int numero){
        this.topic = topic;
        this.numDoc = numDoc;
        this.score = score;
        this.contatoreElementi = 0;
    }

    public Documento(int topic, String numDoc, double score, int numeroRank, String tipo){
        this.topic = topic;
        this.numDoc = numDoc;
        this.score = score;
        this.contaRank = numeroRank;
    }

    public void addRank(int value){
        this.contaRank += value;
    }

    public void setZeroRank(){ this.contaRank = 0; }

    public int getRank(){ return contaRank; }

    public void setScore(double score){
        this.score = score;
    }

    public double getScore(){
        return score;
    }

    public int getTopic(){
        return topic;
    }

    public String getNumDoc(){
        return numDoc;
    }

    public void incrementa(){ contatore++; }

    public void addElement(double score){ this.elementi.add(score); }

    public void trovaMed(){
        int taglia = elementi.size();
        double[] finale = new double[taglia];

        for(int i = 0; i < taglia; i++) {
            finale[i] = elementi.get(i);
        }

        quickSort(finale, 0, taglia -1);

        if((taglia % 2) == 1)
            score = elementi.get(taglia/2);
        else
            score = ((elementi.get(taglia/2) + elementi.get(taglia/2 - 1))/2);
    }

    public void trovaMediaDocumento(){
        this.score = this.score * this.contatore;
    }

    public void trovaMediaDocumentoANZ(){
        this.score = this.score / this.contatore;
    }


    private int partition(double arr[], int left, int right)
    {
        int i = left, j = right;
        double tmp;
        double pivot = arr[(left + right) / 2];

        while (i <= j) {
            while (arr[i] < pivot)
                i++;
            while (arr[j] > pivot)
                j--;
            if (i <= j) {
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
                i++;
                j--;
            }
        }

        return i;
    }

    private void quickSort(double arr[], int left, int right) {
        int index = partition(arr, left, right);
        if (left < index - 1)
            quickSort(arr, left, index - 1);
        if (index < right)
            quickSort(arr, index, right);
    }

    //USARE QUESTO PER I METODI STANDARD
    /*@Override
    public int compareTo(Documento d1)
    {
        if(d1.getTopic() == topic) {
            if (d1.getScore() < this.score) return -1;
            if (d1.getScore() > this.score) return 1;
            return 0;
        }
        if(d1.getTopic() < topic) return -1;
        if(d1.getTopic() > topic) return 1;
        return 0;
    }*/

    // USARE QUESTO PER CONDOR VECCHIO
    /*@Override
    public int compareTo(Documento d1)
    {
        if (d1.getContatoreElementi() < this.contatoreElementi) return -1;
        if (d1.getContatoreElementi() > this.contatoreElementi) return 1;
        return 0;
    }*/

    // USARE QUESTO PER CONDOR NUOVO
    @Override
    public int compareTo(Documento d1)
    {
        if (d1.getRank() < this.contaRank) return -1;
        if (d1.getRank() > this.contaRank) return 1;
        return 0;
    }

    @Override
    public String toString(){
        return "" + this.topic + " " + this.numDoc + " " + this.score;
    }

}
