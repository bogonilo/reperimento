import java.io.*;
import java.util.*;

public class Condor_new {
    public static void main(String[] args) {
        String[] nomeFile = {"BM25", "BM25sw", "DFIZ", "DFIZsw", "InL2", "InL2sw", "PL2", "PL2sw", "TF_IDF", "TF_IDFsw"};
        try {
            BufferedReader[] br = new BufferedReader[10];
            BufferedWriter bw = new BufferedWriter(new FileWriter("runs_results/result/condor_NEW.res"));

            for (int i = 0; i < 10; i++) {
                br[i] = new BufferedReader(new FileReader("runs_results/normalized/normalized" + nomeFile[i] + ".res"));
            }

            int[][] contatori = new int[50][10];
            String riga;
            int contatoreElementi = 0;
            int contatoreTopic = 351;

            int i;
            for (i = 0; i < 10; i++) {
                contatoreTopic = 351;
                int j = 0;

                while ((riga = br[i].readLine()) != null) {

                    Scanner scan = new Scanner(riga);
                    int topic = Integer.parseInt(scan.next());

                    if (topic == contatoreTopic) {
                        contatoreElementi++;
                    } else if (topic == contatoreTopic + 1) {
                        contatoreTopic++;
                        contatori[j][i] = contatoreElementi;
                        contatoreElementi = 1;
                        j++;
                    }
                }
                contatori[j][i] = contatoreElementi;
                contatoreElementi = 0;
            }

            System.out.println("Fine ricerca contatori");

            BufferedReader[] br1 = new BufferedReader[10];
            List<Documento>[] elementiTopic = new List[10];

            for (i = 0; i < 10; i++) {
                br1[i] = new BufferedReader(new FileReader("runs_results/normalized/normalized" + nomeFile[i] + ".res"));
                elementiTopic[i] = new ArrayList<Documento>();
            }

            for(int indiceTopic = 0; indiceTopic < 50; indiceTopic++){
                System.out.println("Topic in analisi: " + (351+indiceTopic));
                List<Documento> allDoc = new ArrayList<>();
                List<Documento> output = new ArrayList<>();

                for(int indiceRun = 0; indiceRun < 10; indiceRun++){
                    elementiTopic[indiceRun] = new ArrayList<>();
                    int numeroRank = contatori[indiceTopic][indiceRun];
                    for(int indiceElementiRun = 0; indiceElementiRun < contatori[indiceTopic][indiceRun]; indiceElementiRun++) {
                        Scanner scan = new Scanner(br1[indiceRun].readLine());
                        int topic = Integer.parseInt(scan.next());
                        String numDoc = scan.next();
                        scan.next();
                        double score = Double.parseDouble(scan.next());

                        Documento doc = new Documento(topic, numDoc, score, numeroRank, "");
                        numeroRank--;
                        elementiTopic[indiceRun].add(doc);

                        boolean trovato = false;
                        for(int ricerca = 0; (ricerca < allDoc.size()) && !trovato; ricerca++){
                            if((topic == allDoc.get(ricerca).getTopic()) && (numDoc.equalsIgnoreCase(allDoc.get(ricerca).getNumDoc()))){
                                trovato = true;
                            }
                        }
                        if(!trovato) {
                            allDoc.add(doc);
                        }
                    }
                }
                /*
                for(int stampa = 0; stampa < 10; stampa++){
                    System.out.println("Arraylist[" + stampa + "] -> " + elementiTopic[stampa].size());
                }
                */
                int numAllDoc = allDoc.size();
                int entraIf = 0;
                for(int indiceAllDoc = 0; (indiceAllDoc < numAllDoc) && (allDoc.size() != 0);){
                    Documento d = allDoc.get(indiceAllDoc);
                    allDoc.remove(d);
                    d.setZeroRank();

                    for(int ricerca = 0; ricerca < 10; ricerca++){
                        for(int ricercaDoc = 0; ricercaDoc < elementiTopic[ricerca].size(); ricercaDoc++){
                            Documento confronto = elementiTopic[ricerca].get(ricercaDoc);
                            if((confronto.getTopic() == d.getTopic()) && confronto.getNumDoc().equalsIgnoreCase(d.getNumDoc())) {
                                d.addRank(confronto.getRank());
                            }
                        }
                    }
                    output.add(d);
                }

                Collections.sort(output);

                //System.out.println("--------------Arraylist[output] -> " + output.size());
                //System.out.println("--------------Arraylist[allDocPostAnalisi] -> " + allDoc.size());


                for(int stampa = 0; (stampa < output.size()) && (stampa < 1000); stampa++)
                {
                    Documento d1 = output.get(stampa);
                    bw.write(d1.getTopic() + " Q0 " + d1.getNumDoc() + " " + stampa + " " + d1.getRank() + " condor \n");
                    bw.flush();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}