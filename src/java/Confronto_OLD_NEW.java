import java.io.*;
import java.util.*;

public class Confronto_OLD_NEW {
    public static void main(String[] args) {

        try {
            BufferedReader brOld = new BufferedReader(new FileReader("runs_results/result/combMed.res"));
            BufferedReader brNew = new BufferedReader(new FileReader("runs_results/result/condor_NEW.res"));
            String rigaOld = "";
            String rigaNew = "";
            int contatore = 0;
            int confrontoEsatto = 0;
            int confronto = 0;
            List<Documento> old = new ArrayList<>();
            List<Documento> nuovo = new ArrayList<>();

            int contatoreScorri = 0;
            while (((rigaOld = brOld.readLine()) != null) && (rigaNew = brNew.readLine()) != null && contatoreScorri < 4000) {
                if(contatoreScorri > 3000) {
                    contatore++;
                    Scanner scanOld = new Scanner(rigaOld);
                    int topicOld = Integer.parseInt(scanOld.next());
                    scanOld.next();
                    String numDocOld = scanOld.next();

                    old.add(new Documento(topicOld, numDocOld, 0));

                    Scanner scanNew = new Scanner(rigaNew);
                    int topicNew = Integer.parseInt(scanNew.next());
                    scanNew.next();
                    String numDocNew = scanNew.next();

                    nuovo.add(new Documento(topicNew, numDocNew, 0));

                    if ((topicNew == topicOld) && numDocNew.equalsIgnoreCase(numDocOld)) {
                        confrontoEsatto++;
                    }
                }
                contatoreScorri++;
            }

            for(int i = 0; i < old.size(); i++){
                Documento d1 = old.get(i);

                for(int j = 0; j < nuovo.size(); j++){
                    Documento d2 = nuovo.get(j);

                    if((d2.getNumDoc().equalsIgnoreCase(d1.getNumDoc())) && (d2.getTopic() == d1.getTopic())){
                        confronto++;
                    }
                }

            }
            System.out.println("Ho confrontato #righe: " + contatore);
            System.out.println("Ho trovato corrispondenza tra " + confronto + " documenti, ma solo " + confrontoEsatto + " hanno lo stesso rank");


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}