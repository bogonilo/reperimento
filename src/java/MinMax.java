import java.io.*;
import java.util.Scanner;

public class MinMax
{
    /**
     * Non per tutti i topic ci sono 1000 risultati (???)
     * Per il topic 369 ci sono solo 70 risultati(???)
     */
    public static void main(String[] args)
    {
        String nomeFile;
        String riga;
        try
        {
            BufferedReader brInput = new BufferedReader(new InputStreamReader(System.in));
            BufferedReader br;
            BufferedWriter bw2;
            Scanner s;
            while(true)
            {
                System.out.println("Inserisci il nome del file da poollare: ");
                nomeFile = brInput.readLine();

                br = new BufferedReader(new FileReader("runs_results/run/" + nomeFile));
                bw2 = new BufferedWriter(new FileWriter("runs_results/min_max/" + nomeFile + "_min_max"));

                int contatoreTopic = 351;
                String stampa = "";
                String score = "";
                int contatore = -1;

                while ((riga = br.readLine()) != null)
                {
                    s = new Scanner(riga);
                    int topicAttuale = Integer.parseInt(s.next());

                    if(topicAttuale == contatoreTopic + 1)
                    {
                        stampa += score + "\n";
                        bw2.write(stampa);
                        bw2.flush();
                        System.out.println("Riga " + contatore + " per topic n°: " + contatoreTopic);
                        contatoreTopic++;
                    }

                    s.next();
                    String idDoc = s.next();
                    contatore = Integer.parseInt(s.next());
                    score = s.next();

                    if(contatore == 0)
                    {
                        stampa = topicAttuale + " " + score + " ";
                    }
                }

                stampa += score + "\n";
                bw2.write(stampa);
                bw2.flush();
                System.out.println("Riga " + contatore + " per topic n°: " + contatoreTopic);
                contatoreTopic++;
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
